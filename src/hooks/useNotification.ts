import { useAppDispatch } from "../redux/hooks";
import {
  notificationActions,
  TNotificationSeverity,
} from "../redux/slices/notificationSlice";

export const useNotification = () => {
  const dispatch = useAppDispatch();

  const showNotification = (
    message: string,
    severity: TNotificationSeverity
  ) => {
    dispatch(notificationActions.show({ message, severity }));
  };

  return { showNotification };
};
