import { useDispatch } from "react-redux";
import { notificationActions } from "../../redux/slices/notificationSlice";

export const useHandleError = () => {
  const dispatch = useDispatch();

  const handleError = (error: any) => {
    if (error.response) {
      dispatch(
        notificationActions.show({
          message: (error as any).response.data.message,
          severity: "error",
        })
      );
    } else {
      dispatch(
        notificationActions.show({
          message: error.message,
          severity: "error",
        })
      );
    }
  };

  return { handleError };
};
