import React from "react";
import { useQuery } from "react-query";
import { useSelector } from "react-redux";

import { adminApi, api } from "../../api";
import { userSelector } from "../../redux/slices/userSlice";
import { IApplication } from "../../types/application.type";
import { useHandleError } from "../helper/useHandleError";

export const useApplications = (isAdmin: boolean = false) => {
  const [page, setPage] = React.useState(0);
  const [perPage, setPerPage] = React.useState(10);

  const { handleError } = useHandleError();

  const user = useSelector(userSelector);

  const queryData = useQuery(["applications", page, perPage], () =>
    (isAdmin ? adminApi.getAllApplications : api.getAllApplications)({
      page: page + 1,
      perPage,
      ...(!isAdmin && {
        [user.role === "CANDIDATE" ? "candidateId" : "recruiterId"]: user.id,
      }),
    })
  );

  if (queryData.isError) handleError(queryData.error);

  const applications: IApplication[] | undefined = queryData.data?.data?.data;
  const totalItems = queryData.data?.data?.meta.pagination.total || 0;
  const totalPages = queryData.data?.data?.meta.pagination.totalPages || 0;

  return {
    page,
    setPage,
    perPage,
    setPerPage,
    applications,
    totalItems,
    totalPages,
    ...queryData,
  };
};
