import { useMutation } from "react-query";
import { adminApi } from "../../api";
import { useHandleError } from "../helper/useHandleError";

export const useDeleteApplication = (onDelete?: () => any) => {
  const { isLoading, isSuccess, isError, error, mutate, reset } = useMutation(
    adminApi.deleteApplication
  );

  const { handleError } = useHandleError();

  if (isError) handleError(error);

  if (isSuccess && onDelete) {
    reset();
    onDelete();
  }

  return { isLoading, isSuccess, isError, error, deleteApplication: mutate };
};
