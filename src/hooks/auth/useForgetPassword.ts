import { UseFormSetError } from "react-hook-form";
import { useMutation } from "react-query";
import { useDispatch } from "react-redux";
import { api } from "../../api";
import { notificationActions } from "../../redux/slices/notificationSlice";
import { IForgetPasswordFormFields } from "../../types";

export const useForgetPassword = (
  setError: UseFormSetError<IForgetPasswordFormFields>
) => {
  const dispatch = useDispatch();

  const { isLoading, mutateAsync } = useMutation(api.forgetPassword);

  const forgetPassword = async (data: { email: string }) => {
    try {
      await mutateAsync(data);
      dispatch(
        notificationActions.show({
          message: `Reset password mail sent to ${data.email}`,
          severity: "success",
        })
      );
    } catch (e: any) {
      if (!e.response) {
        dispatch(
          notificationActions.show({ message: e.message, severity: "error" })
        );
      } else if (e.response.status === 404) {
        setError("email", { type: "manual", message: "Email not found" });
      } else {
        dispatch(
          notificationActions.show({
            message: e.response.message,
            severity: "error",
          })
        );
      }
    }
  };

  return { isLoading, forgetPassword };
};
