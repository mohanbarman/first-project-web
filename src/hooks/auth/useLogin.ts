import { UseFormSetError } from "react-hook-form";
import { useMutation } from "react-query";
import { useHistory } from "react-router-dom";

import { adminApi, api } from "../../api";
import { ILoginData } from "../../api/api";
import { useAppDispatch } from "../../redux/hooks";
import { authActions } from "../../redux/slices/authSlice";
import { notificationActions } from "../../redux/slices/notificationSlice";
import { userActions } from "../../redux/slices/userSlice";
import { ILoginFormFields } from "../../types";

interface ILoginOptions {
  isAdmin: boolean;
}

export const useLogin = (
  setError: UseFormSetError<ILoginFormFields>,
  options: ILoginOptions = { isAdmin: false }
) => {
  const dispatch = useAppDispatch();
  const { isLoading, mutateAsync, error, isError } = useMutation(
    options.isAdmin ? adminApi.login : api.login
  );
  const history = useHistory();

  const login = async (data: ILoginData) => {
    try {
      const res = await mutateAsync(data);
      const { token, ...user } = res.data.data;

      dispatch(
        notificationActions.show({
          message: "Logged in successfully",
          severity: "success",
        })
      );
      dispatch(userActions.setUser(user));
      dispatch(authActions.setToken(token));
      dispatch(authActions.setAuthenticated());
      history.push("/app");
    } catch (e: any) {
      if (!e.response) {
        dispatch(
          notificationActions.show({ message: e.message, severity: "error" })
        );
      } else if (e.response.data.message) {
        setError("email", {
          type: "manual",
          message: e.response.data.message,
        });
      }
    }
  };

  return { login, isLoading, error, isError };
};
