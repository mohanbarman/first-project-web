import { useDispatch } from "react-redux";
import { authActions } from "../../redux/slices/authSlice";
import { userActions } from "../../redux/slices/userSlice";

export const useLogout = () => {
  const dispatch = useDispatch();

  const logout = () => {
    dispatch(authActions.setUnauthenticated());
    dispatch(authActions.removeToken());
    dispatch(userActions.removeUser());
  };

  return { logout };
};
