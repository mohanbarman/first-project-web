import { useMutation } from "react-query";
import { api } from "../../api";
import { useAppDispatch } from "../../redux/hooks";
import { notificationActions } from "../../redux/slices/notificationSlice";

export const useResetPassword = () => {
  const { mutate, isLoading, data, error, isSuccess, reset } = useMutation(
    api.resetPassword
  );

  const dispatch = useAppDispatch();

  if (data?.status === 200) {
    dispatch(
      notificationActions.show({
        message: "Password changed successfully",
        severity: "success",
      })
    );
  } else if ((error as any)?.response) {
    dispatch(
      notificationActions.show({
        message: (error as any).response?.data.message,
        severity: "error",
      })
    );
  } else if (error) {
    dispatch(
      notificationActions.show({
        message: (error as any).message,
        severity: "error",
      })
    );
  }

  return { mutate, isLoading, error, isSuccess, reset };
};
