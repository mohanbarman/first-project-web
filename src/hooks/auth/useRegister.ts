import { UseFormSetError } from "react-hook-form";
import { useMutation } from "react-query";
import { useHistory } from "react-router-dom";
import { api } from "../../api";
import { useAppDispatch } from "../../redux/hooks";
import { authActions } from "../../redux/slices/authSlice";
import { notificationActions } from "../../redux/slices/notificationSlice";
import { userActions } from "../../redux/slices/userSlice";
import { IRegisterFormFields, IUser } from "../../types";

export const useRegister = (setError: UseFormSetError<IRegisterFormFields>) => {
  const dispatch = useAppDispatch();
  const { mutateAsync, isLoading, isError, error } = useMutation(api.register);
  const history = useHistory();

  const register = async (
    data: IRegisterFormFields & { role: IUser["role"] }
  ) => {
    try {
      const res = await mutateAsync(data);
      const { token, ...user } = res.data.data;
      dispatch(userActions.setUser(user));
      dispatch(authActions.setToken(token));
      dispatch(authActions.setAuthenticated());
      dispatch(
        notificationActions.show({
          message: "You've registered successfully",
          severity: "success",
        })
      );
      history.push("/app");
    } catch (e: any) {
      if (!e.response) {
        dispatch(
          notificationActions.show({ message: e.message, severity: "error" })
        );
      } else if (e.response.status === 409) {
        setError("email", {
          type: "manual",
          message: "This email address is already in use",
        });
      }
    }
  };

  return { register, isLoading, isError, error };
};
