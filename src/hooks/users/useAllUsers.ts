import React from "react";
import { useQuery } from "react-query";
import { adminApi } from "../../api";
import { IUser } from "../../types";
import { useHandleError } from "../helper/useHandleError";

export const useAllUsers = () => {
  const [page, setPage] = React.useState(0);
  const [perPage, setPerPage] = React.useState(10);
  const [role, setRole] = React.useState<IUser["role"] | "">("");

  const { handleError } = useHandleError();

  const { isLoading, isError, data, error, refetch } = useQuery(
    ["users", page, perPage, role],
    () =>
      adminApi.getAllUsers({
        ...(role ? { role } : {}),
        ...{ page: page + 1, perPage },
      })
  );

  if (isError) handleError(error);

  const fetchPage = (page: number) => {
    setPage(page);
  };
  const changePerPage = (perPage: number) => setPerPage(perPage);
  const changeRole = (role: IUser["role"]) => setRole(role);

  const users: IUser[] = data?.data?.data || [];
  const totalItems: number = data?.data?.meta.pagination.total || 0;
  const totalPages: number = data?.data?.meta.pagination.totalPages || 0;

  return {
    isLoading,
    page,
    perPage,
    role,
    users,
    totalItems,
    totalPages,
    fetchPage,
    changeRole,
    changePerPage,
    refetch,
  };
};
