import { useMutation } from "react-query";
import { adminApi } from "../../api";
import { useAppDispatch } from "../../redux/hooks";
import { notificationActions } from "../../redux/slices/notificationSlice";
import { useHandleError } from "../helper/useHandleError";

interface IArgs {
  onDelete: () => any;
}

export const useDeleteUser = (args: IArgs) => {
  const { mutate, isSuccess, error, isLoading, isError, reset } = useMutation(
    adminApi.deleteUser
  );
  const dispatch = useAppDispatch();
  const { handleError } = useHandleError();

  const deleteUser = (userId: string) => {
    mutate(userId);
  };

  if (isSuccess) {
    dispatch(
      notificationActions.show({
        message: "User deleted successfully",
        severity: "success",
      })
    );
    args.onDelete();
    reset();
  }

  if (isError) handleError(error);

  return { deleteUser, isLoading };
};
