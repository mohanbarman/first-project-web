import { useMutation } from "react-query";
import { api } from "../../api";
import { ICreateJobData } from "../../api/api";
import { useAppDispatch } from "../../redux/hooks";
import { notificationActions } from "../../redux/slices/notificationSlice";

export const useAddJob = () => {
  const { isLoading, mutateAsync, isSuccess, reset } = useMutation(
    api.createJob
  );

  const dispatch = useAppDispatch();

  const createJob = async (data: ICreateJobData) => {
    try {
      const res = await mutateAsync(data);
      if (res.status === 200) {
        dispatch(
          notificationActions.show({
            message: "Job created successfully",
            severity: "success",
          })
        );
      }
    } catch (e: any) {
      dispatch(
        notificationActions.show({
          message: e.response?.data?.message || e.message,
          severity: "error",
        })
      );
    }
  };

  return { createJob, isLoading, isSuccess, reset };
};
