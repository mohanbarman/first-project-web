import { useMutation } from "react-query";
import { useDispatch } from "react-redux";
import { api } from "../../api";
import { notificationActions } from "../../redux/slices/notificationSlice";
import { useHandleError } from "../helper/useHandleError";

export const useApplyJob = (onApply?: () => any) => {
  const { mutate, isSuccess, isLoading, isError, error, reset } = useMutation(
    api.createApplication,
    { retry: false }
  );
  const { handleError } = useHandleError();

  const dispatch = useDispatch();

  const applyJob = (jobId: string) => {
    mutate(jobId);
  };

  if (isError) {
    handleError(error);
    reset();
  }

  if (isSuccess) {
    dispatch(
      notificationActions.show({
        message: "Successfully applied for job",
        severity: "success",
      })
    );
    onApply?.();
    reset();
  }

  return { isLoading, applyJob, isSuccess };
};
