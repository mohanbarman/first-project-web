import React, { useEffect } from "react";
import { useQuery } from "react-query";
import { api } from "../../api";
import { IJob } from "../../types/job.type";
import { useHandleError } from "../helper/useHandleError";

interface IArgs {
  candidateId?: string;
  recruiterId?: string;
}

export const useAllJobs = (queryParams?: IArgs) => {
  const [page, setPage] = React.useState(1);
  const [perPage, setPerPage] = React.useState(10);
  const [searchQuery, setSearchQuery] = React.useState("");
  const { handleError } = useHandleError();

  const { isLoading, isError, data, error, refetch } = useQuery(
    ["jobs", page, perPage],
    () => api.getAllJobs({ page, perPage, q: searchQuery, ...queryParams })
  );

  if (error) handleError(error);

  useEffect(() => {
    refetch();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [searchQuery]);

  const fetchNextPage = () => setPage((p) => p + 1);
  const fetchPage = (page: number) => setPage(page);
  const search = (q: string) => {
    setSearchQuery(q);
  };
  const changePerPage = (page: number) => setPerPage(page);

  const jobs: IJob[] = data && data.data.data;
  const totalItems: number = data?.data?.meta.pagination.total || 0;
  const totalPages: number = data?.data?.meta.pagination.totalPages || 0;

  return {
    isLoading,
    isError,
    jobs,
    page,
    totalItems,
    totalPages,
    fetchNextPage,
    fetchPage,
    search,
    searchValue: searchQuery,
    perPage,
    refetch,
    changePerPage,
  };
};
