import { useMutation } from "react-query";
import { adminApi } from "../../api";
import { useAppDispatch } from "../../redux/hooks";
import { notificationActions } from "../../redux/slices/notificationSlice";
import { useHandleError } from "../helper/useHandleError";

export const useDeleteJob = (onDelete: () => any) => {
  const { error, isError, isSuccess, isLoading, reset, mutateAsync } =
    useMutation(adminApi.deleteJob);

  const { handleError } = useHandleError();
  const dispatch = useAppDispatch();

  if (isError) handleError(error);

  if (isSuccess) {
    dispatch(
      notificationActions.show({
        message: "Job deleted successfully",
        severity: "success",
      })
    );
    reset();
    onDelete();
  }

  const deleteJob = (jobId: string) => {
    console.log(jobId);
    mutateAsync(jobId);
  };

  return { deleteJob, isLoading };
};
