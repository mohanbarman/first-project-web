import { useMutation } from "react-query";
import { useDispatch, useSelector } from "react-redux";
import { api } from "../api";
import { authSelector, authActions } from "../redux/slices/authSlice";
import { userActions } from "../redux/slices/userSlice";

export const useAuth = () => {
  const { token, status } = useSelector(authSelector);
  const dispatch = useDispatch();

  const { mutateAsync } = useMutation(api.getMe);

  const getMe = async () => {
    if (!token) {
      dispatch(authActions.setUnauthenticated());
      return;
    }
    try {
      const res = await mutateAsync();
      const user = res.data.data;
      dispatch(userActions.setUser(user));
      dispatch(authActions.setAuthenticated());
    } catch (e: any) {
      dispatch(authActions.setUnauthenticated());
    }
  };

  return { status, getMe };
};
