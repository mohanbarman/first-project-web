import { IUser } from "../types";
import {
  Api,
  IBasePagination,
  IGetAllApplicationsData,
  ILoginData,
} from "./api";

export const login = (data: ILoginData) => {
  return Api.post("/admin/auth/login", data);
};

interface IGetAllUsersData extends IBasePagination {
  q?: string;
  name?: string;
  role?: IUser["role"];
}

export const getAllUsers = (data: IGetAllUsersData) => {
  return Api.get("/admin/users", { params: data });
};

export const deleteUser = (userId: string) => {
  return Api.delete("/admin/users/" + userId);
};

export const getAllApplications = (data: IGetAllApplicationsData) => {
  return Api.get("/admin/applications", { params: data });
};

export const deleteJob = (jobId: string) => {
  return Api.delete("/admin/jobs/" + jobId);
};

export const deleteApplication = (id: string) => {
  return Api.delete("/admin/applications/" + id);
};
