import axios from "axios";
import { IUser } from "../types";

export interface IBasePagination {
  page: number;
  perPage: number;
}

export interface ISearchQuery {
  q: string;
}

const authorizationToken = localStorage.getItem("token");

export const Api = axios.create({
  baseURL: process.env.REACT_APP_BASE_URL,
  headers: authorizationToken && {
    Authorization: `Bearer ${authorizationToken}`,
  },
});

export const clearTokenFromAxios = () => {
  Api.defaults.headers = {};
};

export const setTokenOnAxios = (token: string) => {
  Api.defaults.headers = { Authorization: `Bearer ${token}` };
};

// LOGIN API
export interface ILoginData {
  email: string;
  password: string;
  reToken?: string;
}

export const login = async (data: ILoginData) => {
  return Api.post("/auth/login", data);
};

// REGISTER API
interface IRegisterData extends ILoginData {
  name: string;
  role: IUser["role"];
}

export const register = async (data: IRegisterData) => {
  return Api.post("/auth/register", data);
};

// GET LOGGED IN USER API
export const getMe = async () => {
  return Api.get("/auth/me");
};

// FORGET PASSWORD API
export const forgetPassword = async (data: { email: string }) => {
  return Api.post("/auth/forgetPassword", data);
};

// RESET PASSWORD API
export interface IResetPasswordData {
  secret: string;
  password: string;
}

export const resetPassword = async (data: IResetPasswordData) => {
  return Api.post("/auth/resetPassword", data);
};

// GET ALL APPLICATIONS API
export interface IGetAllApplicationsData extends Partial<IBasePagination> {
  jobId?: string;
  recruiterId?: string;
  candidateId?: string;
}

export const getAllApplications = async (data: IGetAllApplicationsData) => {
  return Api.get("/applications", { params: data });
};

// CREATE JOB API
export interface ICreateJobData {
  title: string;
  description: string;
}

export const createJob = async (data: ICreateJobData) => {
  return Api.post("/jobs", data);
};

// GET ALL JOBS API
export type TGetAllJobsData = IBasePagination &
  ISearchQuery & {
    candidateId?: string;
    recruiterId?: string;
  };

export const getAllJobs = async (data: TGetAllJobsData) => {
  return Api.get("/jobs", { params: data });
};

// CREATE NEW APPLICATION API
export const createApplication = async (jobId: string) => {
  return Api.post("/applications", { jobId });
};
