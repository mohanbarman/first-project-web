import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { RootState } from "../store";

export type TNotificationSeverity = "error" | "warning" | "info" | "success";

interface INotificationState {
  severity: TNotificationSeverity;
  open: boolean;
  message: string;
}

const initialState: INotificationState = {
  severity: "success",
  open: false,
  message: "",
};

export type TPayloadAction = PayloadAction<Omit<INotificationState, "open">>;

export const notificationSlice = createSlice({
  name: "notification",
  initialState,
  reducers: {
    show: (state, action: TPayloadAction) => {
      state.open = true;
      state.severity = action.payload.severity;
      state.message = action.payload.message;
    },
    hide: (state) => {
      state.open = false;
      state.severity = "success";
      state.message = "";
    },
  },
});

export const notificationActions = notificationSlice.actions;
export const notificationSelector = (state: RootState) => state.notification;
export default notificationSlice.reducer;
