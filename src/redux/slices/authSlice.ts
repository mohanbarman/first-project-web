import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { api } from "../../api";
import { clearTokenFromAxios } from "../../api/api";

import { RootState } from "../store";

type TAuthStatus = "authenticated" | "unauthenticated" | "loading";

interface IInitialState {
  token: string;
  status: TAuthStatus;
}

const initialState: Partial<IInitialState> = {
  token: localStorage.getItem("token") || "",
  status: "loading",
};

const authSlice = createSlice({
  name: "auth",
  initialState,
  reducers: {
    setAuthenticated: (state) => {
      state.status = "authenticated";
    },
    setUnauthenticated: (state) => {
      state.token = "";
      state.status = "unauthenticated";
    },
    setToken: (state, action: PayloadAction<string>) => {
      state.status = "authenticated";
      state.token = action.payload;
      localStorage.setItem("token", action.payload);
      api.setTokenOnAxios(action.payload);
    },
    removeToken: (state) => {
      state.token = "";
      localStorage.removeItem("token");
      clearTokenFromAxios();
    },
  },
});

export const authActions = authSlice.actions;
export const authSelector = (state: RootState) => state.auth;
export default authSlice.reducer;
