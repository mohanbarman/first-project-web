import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { IUser } from "../../types";
import { localStorageApi } from "../../utils";
import { RootState } from "../store";

const initialState: Partial<IUser> =
  localStorageApi.getObjectByKey<IUser>("user");

const userSlice = createSlice({
  name: "user",
  initialState,
  reducers: {
    setUser: (state, action: PayloadAction<IUser>) => {
      state.id = action.payload.id;
      state.createdAt = action.payload.createdAt;
      state.updatedAt = action.payload.updatedAt;
      state.email = action.payload.email;
      state.role = action.payload.role;
      localStorageApi.setObjectByKey("user", state);
    },
    removeUser: (state) => {
      delete state.id;
      delete state.createdAt;
      delete state.updatedAt;
      delete state.email;
      delete state.role;
      localStorage.removeItem("user");
    },
  },
});

export const userActions = userSlice.actions;
export const userSelector = (state: RootState) => state.user;
export default userSlice.reducer;
