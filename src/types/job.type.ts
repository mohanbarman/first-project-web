import { IUser } from "./user.type";

export interface IJob {
  id: string;
  createdAt: Date;
  updatedAt: Date;
  title: string;
  description: string;
  createdByUserId: string;
  recruiter: IUser;
  isApplied?: boolean;
}
