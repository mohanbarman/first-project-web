export interface IUser {
  id: string;
  name: string;
  email: string;
  role: "CANDIDATE" | "RECRUITER" | "ADMIN";
  createdAt: Date;
  updatedAt: Date;
}
