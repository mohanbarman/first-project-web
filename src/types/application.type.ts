import { IUser } from ".";
import { IJob } from "./job.type";

export interface IApplication {
  id: string;
  createdAt: string;
  updatedAt: string;
  jobId: string;
  candidateId: string;
  job: IJob;
  candidate: IUser;
}
