export interface IForgetPasswordFormFields {
  email: string;
}

export interface ILoginFormFields {
  email: string;
  password: string;
}

export interface IRegisterFormFields {
  name: string;
  email: string;
  password: string;
  confirmPassword: string;
}
