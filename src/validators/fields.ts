import * as yup from "yup";

export const requiredField = yup
  .string()
  .typeError("This field must be an string")
  .required("This field is required");

export const passwordField = requiredField
  .min(8, "Password must be greater than 8 characters")
  .max(20, "Password must be smaller than 20 characters");

export const emailField = requiredField.email("Please enter a valid email");
