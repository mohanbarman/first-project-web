import styled from "styled-components";

export const FormContainer = styled.div`
  display: flex;
  flex-direction: column;
  gap: 20px;
  max-width: 600px;
  margin: auto;
  padding: 20px;
  min-height: calc(100vh - 200px);
  justify-content: center;
`;
