import styled from "styled-components";

export const Container = styled.div`
  padding: 10px;
  margin-top: 64px;

  @media (max-width: 600px) {
    margin-top: 56px;
  }
`;
