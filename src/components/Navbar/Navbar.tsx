import {
  AppBar,
  Button,
  Drawer,
  Hidden,
  IconButton,
  List,
  ListItem,
  ListItemText,
  makeStyles,
  Toolbar,
  Typography,
} from "@material-ui/core";
import MenuIcon from "@material-ui/icons/Menu";
import React from "react";
import { useState } from "react";
import { Link, useHistory } from "react-router-dom";
import { useLogout } from "../../hooks/auth/useLogout";
import { useAppSelector } from "../../redux/hooks";
import { authSelector } from "../../redux/slices/authSlice";
import { userSelector } from "../../redux/slices/userSlice";
import { Container } from "./styled";

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  menuButton: {
    marginRight: theme.spacing(2),
  },
  title: {
    flexGrow: 1,
  },
}));

interface IProps {
  children: React.ReactElement;
}

export const Navbar: React.FC<IProps> = ({ children }) => {
  const classes = useStyles();
  const [isDrawerOpen, setIsDrawerOpen] = useState(false);

  const { status } = useAppSelector(authSelector);
  const { role } = useAppSelector(userSelector);

  const { logout } = useLogout();
  const history = useHistory();

  const handleLogoutClick = () => {
    logout();
    history.push("/");
  };

  return (
    <>
      <AppBar color={role === "ADMIN" ? "secondary" : "primary"}>
        <Toolbar>
          <Typography variant="h6" className={classes.title}>
            <Link to={status === "authenticated" ? "/app" : "/"}>MY JOBS</Link>
          </Typography>
          {status === "authenticated" && role === "CANDIDATE" && (
            <Hidden only="xs">
              <Link to="/appliedJobs">
                <Button color="inherit">Applied jobs</Button>
              </Link>
            </Hidden>
          )}
          {status === "unauthenticated" && (
            <Hidden only="xs">
              <Link to="/login">
                <Button color="inherit">Login</Button>
              </Link>
              <Link to="/register">
                <Button color="inherit">Register</Button>
              </Link>
            </Hidden>
          )}
          {status === "authenticated" && (
            <Hidden only="xs">
              <Button color="inherit" onClick={handleLogoutClick}>
                Logout
              </Button>
            </Hidden>
          )}
          <Hidden smUp>
            <IconButton color="inherit" onClick={() => setIsDrawerOpen(true)}>
              <MenuIcon />
            </IconButton>
            <Drawer
              anchor="right"
              open={isDrawerOpen}
              onClose={() => setIsDrawerOpen(false)}
            >
              <List>
                {status === "unauthenticated" && (
                  <>
                    <Link to="/login">
                      <ListItem button>
                        <ListItemText primary="Login" />
                      </ListItem>
                    </Link>
                    <Link to="/register">
                      <ListItem button>
                        <ListItemText primary="Register" />
                      </ListItem>
                    </Link>
                  </>
                )}
                {status === "authenticated" && (
                  <>
                    <ListItem button>
                      <ListItemText primary="Logout" />
                    </ListItem>
                  </>
                )}
              </List>
            </Drawer>
          </Hidden>
        </Toolbar>
      </AppBar>
      <Container>{children}</Container>
    </>
  );
};
