import React from "react";
import { Helmet } from "react-helmet";

interface IProps {
  title: string;
  description: string;
}

export const HtmlTitle: React.FC<IProps> = ({ title, description }) => {
  return (
    <Helmet>
      <title>{title}</title>
      <meta name="og:title" content={title} />
      <meta name="og:description" content={description} />
    </Helmet>
  );
};
