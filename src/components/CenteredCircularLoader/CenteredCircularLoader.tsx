import React from "react";
import styled from "styled-components";
import { CircularProgress } from "@material-ui/core";

export const CenteredCircularLoader: React.FC = () => (
  <Container>
    <CircularProgress color="primary" />
  </Container>
);

const Container = styled.div`
  height: 100vh;
  width: 100vw;
  display: flex;
  align-items: center;
  justify-content: center;
`;
