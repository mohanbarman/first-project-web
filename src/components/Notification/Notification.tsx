import { Snackbar } from "@material-ui/core";
import { Alert } from "@material-ui/lab";
import React from "react";
import { useDispatch } from "react-redux";

import { useAppSelector } from "../../redux/hooks";
import {
  notificationSelector,
  notificationActions,
} from "../../redux/slices/notificationSlice";

export const Notification: React.FC = () => {
  const { message, open, severity } = useAppSelector(notificationSelector);
  const dispatch = useDispatch();

  const handleClose = () => dispatch(notificationActions.hide());

  return (
    <Snackbar
      open={open}
      anchorOrigin={{ horizontal: "center", vertical: "top" }}
      autoHideDuration={6000}
      onClose={handleClose}
    >
      <Alert onClose={handleClose} severity={severity}>
        {message}
      </Alert>
    </Snackbar>
  );
};
