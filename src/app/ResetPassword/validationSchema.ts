import * as yup from "yup";
import { passwordField, requiredField } from "../../validators";

export const resetPasswordSchema = yup.object().shape({
  password: passwordField,
  confirmPassword: requiredField.oneOf(
    [yup.ref("password")],
    "Password didn't matched"
  ),
});
