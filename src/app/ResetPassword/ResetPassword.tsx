import { yupResolver } from "@hookform/resolvers/yup";
import { Button, TextField, Typography } from "@material-ui/core";
import React from "react";
import { SubmitHandler } from "react-hook-form";
import { useForm } from "react-hook-form";
import { useParams } from "react-router-dom";
import FormContainer from "../../components/FormContainer";
import Navbar from "../../components/Navbar";
import { registerTextField } from "../../utils";
import { useResetPassword } from "../../hooks/auth/useResetPassword";
import { resetPasswordSchema } from "./validationSchema";
import { HtmlTitle } from "../../components/HtmlTitle";

export interface IFormFields {
  password: string;
  confirmPassword: string;
}

export const ResetPassword: React.FC = () => {
  const {
    handleSubmit,
    register,
    formState: { errors },
    reset,
  } = useForm<IFormFields>({ resolver: yupResolver(resetPasswordSchema) });

  const {
    mutate,
    isLoading,
    isSuccess,
    reset: resetRequestState,
  } = useResetPassword();

  const { secretCode } = useParams<{ secretCode: string }>();

  const onSubmit: SubmitHandler<IFormFields> = ({ password }) => {
    mutate({ password, secret: secretCode });
  };

  if (isSuccess) {
    reset();
    resetRequestState();
  }

  return (
    <>
      <HtmlTitle title="Reset password" description="Reset password" />
      <Navbar>
        <form onSubmit={handleSubmit(onSubmit)}>
          <FormContainer>
            <Typography align="center" variant="h5">
              Reset your password
            </Typography>
            <TextField
              {...registerTextField(register, errors, "password")}
              disabled={isLoading}
              variant="outlined"
              type="password"
              label="New password"
            />
            <TextField
              {...registerTextField(register, errors, "confirmPassword")}
              disabled={isLoading}
              variant="outlined"
              type="password"
              label="Confirm password"
            />
            <Button
              disabled={isLoading}
              type="submit"
              color="primary"
              variant="contained"
            >
              {isLoading ? "Please wait..." : "Submit"}
            </Button>
          </FormContainer>
        </form>
      </Navbar>
    </>
  );
};
