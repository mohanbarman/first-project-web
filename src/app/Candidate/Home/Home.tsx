import React from "react";
import { HtmlTitle } from "../../../components/HtmlTitle";
import Navbar from "../../../components/Navbar";
import { Jobs } from "./Jobs";

export const CandidateHome: React.FC = () => {
  return (
    <>
      <HtmlTitle title="Homepage" description="Homepage" />
      <Navbar>
        <Jobs />
      </Navbar>
    </>
  );
};
