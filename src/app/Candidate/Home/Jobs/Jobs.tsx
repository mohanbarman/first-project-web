import { CircularProgress, TextField } from "@material-ui/core";
import { Pagination } from "@material-ui/lab";
import React from "react";
import { useAllJobs } from "../../../../hooks/job/useAllJobs";
import { Job } from "./Job";
import { RootContainer, Container, JobsContainer } from "./styled";

export const Jobs: React.FC = () => {
  const {
    isLoading,
    page,
    totalPages,
    fetchPage,
    jobs,
    searchValue,
    search,
    refetch,
  } = useAllJobs();

  if (isLoading || !jobs) return <CircularProgress color="primary" />;

  return (
    <RootContainer>
      <TextField
        value={searchValue}
        onChange={(e) => search(e.target.value)}
        fullWidth
        variant="outlined"
        margin="dense"
        placeholder="Search job"
        style={{ maxWidth: "700px", marginBottom: "30px" }}
      />
      <Container>
        <JobsContainer>
          {jobs.map((job) => (
            <Job key={job.id} onApply={refetch} job={job} />
          ))}
        </JobsContainer>
        <Pagination
          style={{ marginTop: "20px" }}
          count={totalPages}
          page={page}
          onChange={(_, value) => fetchPage(value)}
        />
      </Container>
    </RootContainer>
  );
};
