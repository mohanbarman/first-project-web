import { Typography } from "@material-ui/core";
import styled from "styled-components";

export const RootContainer = styled.div`
  padding: 30px;
  display: flex;
  flex-direction: column;
  align-items: center;

  .MuiPagination-root {
    justify-content: center;
    display: flex;
  }
`;

export const Container = styled.div`
  display: flex;
  flex-direction: column;
  max-width: 700px;
  width: 100%;
`;

export const JobsContainer = styled.div`
  display: flex;
  flex-direction: column;
  gap: 20px;
`;

export const JobDescription = styled(Typography)`
  white-space: pre-wrap; /* Since CSS 2.1 */
  white-space: -moz-pre-wrap; /* Mozilla, since 1999 */
  white-space: -pre-wrap; /* Opera 4-6 */
  white-space: -o-pre-wrap; /* Opera 7 */
  word-wrap: break-word; /* Internet Explorer 5.5+ */
`;
