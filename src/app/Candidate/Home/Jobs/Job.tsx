import { Box, Button, Typography } from "@material-ui/core";
import moment from "moment";
import React from "react";
import { useApplyJob } from "../../../../hooks/job/useApplyJob";
import { IJob } from "../../../../types/job.type";
import { JobDescription } from "./styled";

interface IProps {
  job: IJob;
  onApply?: () => any;
}

export const Job: React.FC<IProps> = ({ job, onApply }) => {
  const { applyJob, isLoading } = useApplyJob(onApply);

  return (
    <Box>
      <Typography variant="h5">{job.title}</Typography>
      <Typography variant="caption">
        Published {moment(job.createdAt).startOf("hour").fromNow()}
      </Typography>
      <JobDescription variant="body1">{job.description}</JobDescription>
      <Button
        style={{ marginTop: "10px" }}
        disabled={isLoading || job.isApplied}
        onClick={() => applyJob(job.id)}
        variant="contained"
        color="primary"
      >
        {job.isApplied ? "Already applied" : "Apply"}
      </Button>
    </Box>
  );
};
