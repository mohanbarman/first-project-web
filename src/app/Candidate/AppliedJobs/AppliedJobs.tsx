import {
  CircularProgress,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TablePagination,
  TableRow,
  Toolbar,
  Typography,
} from "@material-ui/core";
import moment from "moment";
import React from "react";
import { HtmlTitle } from "../../../components/HtmlTitle";
import Navbar from "../../../components/Navbar";
import { useApplications } from "../../../hooks/applications/useAllApplications";
import { RootContainer, Container } from "./styled";

const headers = ["Created At", "Job title", "Job description"];

export const AppliedJobs: React.FC = () => {
  const { applications, page, perPage, setPage, setPerPage, totalItems } =
    useApplications();

  if (!applications) return <CircularProgress color="primary" />;

  return (
    <>
      <HtmlTitle title="Aplied jobs" description="View all applied jobs" />
      <Navbar>
        <RootContainer>
          <Container elevation={6}>
            <TableContainer>
              <Toolbar>
                <Typography variant="h5" id="tableTitle" component="div">
                  All Applications
                </Typography>
              </Toolbar>
              <TableHead>
                <TableRow>
                  {headers.map((header, index) => (
                    <TableCell key={index}>{header}</TableCell>
                  ))}
                </TableRow>
              </TableHead>
              <TableBody>
                {applications.map((application) => (
                  <TableRow key={application.id}>
                    <TableCell>
                      {moment(application.createdAt).format(
                        "MMMM Do YYYY, h:mm:ss a"
                      )}
                    </TableCell>
                    <TableCell>{application.job.title}</TableCell>
                    <TableCell>
                      {application.job.description.slice(0, 40)}
                    </TableCell>
                  </TableRow>
                ))}
              </TableBody>
            </TableContainer>
            <TablePagination
              rowsPerPageOptions={[5, 10, 25]}
              component="div"
              count={totalItems}
              rowsPerPage={perPage}
              page={page}
              onPageChange={(_, page) => setPage(page)}
              onRowsPerPageChange={(e) => setPerPage(parseInt(e.target.value))}
            />
          </Container>
        </RootContainer>
      </Navbar>
    </>
  );
};
