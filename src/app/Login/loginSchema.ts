import * as yup from "yup";
import { emailField, requiredField } from "../../validators/fields";

export const loginSchema = yup.object().shape({
  email: emailField,
  password: requiredField,
});
