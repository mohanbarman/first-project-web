import React from "react";
import { useForm, SubmitHandler } from "react-hook-form";
import { TextField, Button, Typography } from "@material-ui/core";
import AccountCircleIcon from "@material-ui/icons/AccountCircle";
import { Link } from "react-router-dom";
import { yupResolver } from "@hookform/resolvers/yup";

import { Navbar } from "../../components/Navbar/Navbar";
import FormContainer from "../../components/FormContainer";
import { loginSchema } from "./loginSchema";
import { registerTextField } from "../../utils";
import { useLogin } from "../../hooks/auth/useLogin";
import { ILoginFormFields } from "../../types";
import { HtmlTitle } from "../../components/HtmlTitle";

export const Login: React.FC = () => {
  const {
    register,
    handleSubmit,
    setError,
    formState: { errors },
  } = useForm<ILoginFormFields>({
    resolver: yupResolver(loginSchema),
  });

  const { isLoading, login } = useLogin(setError);

  const onSubmit: SubmitHandler<ILoginFormFields> = async (data) => {
    login(data);
  };

  return (
    <>
      <HtmlTitle
        title="Login"
        description="Login using your email and password"
      />
      <Navbar>
        <form onSubmit={handleSubmit(onSubmit)}>
          <FormContainer>
            <AccountCircleIcon
              style={{ margin: "0 auto", fontSize: 50 }}
              color="primary"
            />
            <Typography variant="h5" align="center">
              Login to your account
            </Typography>

            <TextField
              {...registerTextField(register, errors, "email")}
              label="Email"
              variant="outlined"
            />

            <TextField
              {...registerTextField(register, errors, "password")}
              label="Password"
              type="password"
              variant="outlined"
            />

            <Typography>
              Forgot your password ?
              <Link to="/forgetPassword">
                <Button
                  color="secondary"
                  style={{ textTransform: "capitalize" }}
                >
                  forgot password
                </Button>
              </Link>
            </Typography>

            <Button
              disabled={isLoading}
              type="submit"
              color="primary"
              variant="contained"
            >
              {isLoading ? "Loading ..." : "Login"}
            </Button>
            <Typography align="center">
              Don't have an account ?
              <Link to="/register">
                <Button color="primary" style={{ textTransform: "capitalize" }}>
                  Sign up
                </Button>
              </Link>
            </Typography>
          </FormContainer>
        </form>
      </Navbar>
    </>
  );
};
