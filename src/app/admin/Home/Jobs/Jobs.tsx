import {
  TableContainer,
  TableHead,
  TableRow,
  TableCell,
  TableBody,
  CircularProgress,
  Button,
  TablePagination,
} from "@material-ui/core";
import moment from "moment";
import React from "react";

import { useAllJobs } from "../../../../hooks/job/useAllJobs";
import { useDeleteJob } from "../../../../hooks/job/useDeleteJob";
import { RootContainer } from "../styled";

export const Jobs: React.FC = () => {
  const {
    jobs,
    isLoading,
    page,
    perPage,
    totalItems,
    changePerPage,
    fetchPage,
    refetch,
  } = useAllJobs();
  const { deleteJob, isLoading: isDeleting } = useDeleteJob(refetch);

  const headers = [
    "Created At",
    "Updated At",
    "Title",
    "Description",
    "Recruiter Name",
    "Recruiter Email",
    "Actions",
  ];

  if (isLoading) return <CircularProgress />;

  const handleDelete = (id: string, title: string) => {
    if (window.confirm(`Do you want to delete ${title}`)) deleteJob(id);
  };

  return (
    <RootContainer>
      <TableContainer>
        <TableHead>
          <TableRow>
            {headers.map((header) => (
              <TableCell>{header}</TableCell>
            ))}
          </TableRow>
        </TableHead>
        <TableBody>
          {jobs.map((job) => (
            <TableRow key={job.id}>
              <TableCell>
                {moment(job.createdAt).format("MMMM Do YYYY, h:mm:ss a")}
              </TableCell>
              <TableCell>
                {moment(job.updatedAt).format("MMMM Do YYYY, h:mm:ss a")}
              </TableCell>
              <TableCell>{job.title}</TableCell>
              <TableCell>{job.description.slice(0, 50)}</TableCell>
              <TableCell>{job.recruiter.name}</TableCell>
              <TableCell>{job.recruiter.email}</TableCell>
              <TableCell>
                <Button
                  onClick={() => handleDelete(job.id, job.title)}
                  disabled={isDeleting}
                  color="secondary"
                  variant="outlined"
                >
                  Delete
                </Button>
              </TableCell>
            </TableRow>
          ))}
        </TableBody>
        <TablePagination
          rowsPerPageOptions={[5, 10, 25]}
          component="div"
          count={totalItems}
          rowsPerPage={perPage}
          page={page - 1}
          onPageChange={(_, page) => fetchPage(page + 1)}
          onRowsPerPageChange={(e) => changePerPage(parseInt(e.target.value))}
        />
      </TableContainer>
    </RootContainer>
  );
};
