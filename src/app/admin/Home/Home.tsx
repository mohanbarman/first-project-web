import React from "react";
import { Tab, Tabs } from "@material-ui/core";

import Navbar from "../../../components/Navbar";
import Jobs from "./Jobs";
import Applications from "./Applications";
import Users from "./Users";
import { RootContainer } from "./styled";
import { HtmlTitle } from "../../../components/HtmlTitle";

export const AdminHome: React.FC = () => {
  const [tab, setTab] = React.useState(0);

  return (
    <>
      <HtmlTitle
        title="Admin dashboard"
        description="Admin dashboard to view all users, applications, jobs"
      />
      <Navbar>
        <RootContainer>
          <Tabs
            value={tab}
            indicatorColor="secondary"
            textColor="secondary"
            onChange={(_, value) => setTab(value)}
          >
            <Tab label="All Jobs" />
            <Tab label="All Applications" />
            <Tab label="All Users" />
          </Tabs>
          {tab === 0 && <Jobs />}
          {tab === 1 && <Applications />}
          {tab === 2 && <Users />}
        </RootContainer>
      </Navbar>
    </>
  );
};
