import styled from "styled-components";

export const RootContainer = styled.div`
  margin-top: 20px;
  display: flex;
  justify-content: center;
  align-items: center;
  flex-direction: column;
  gap: 30px;

  .MuiTableContainer-root {
    width: unset;
  }
`;
