import {
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  TableSortLabel,
  TablePagination,
  CircularProgress,
  Button,
} from "@material-ui/core";
import React from "react";
import moment from "moment";

import { RootContainer } from "../styled";
import { useApplications } from "../../../../hooks/applications/useAllApplications";
import { useDeleteApplication } from "../../../../hooks/applications/useDeleteApplication";

const headers = [
  "Applied At",
  "Candidate Name",
  "Candidate Email",
  "Job Title",
  "Job Description",
  "Actions",
];

export const Applications: React.FC = () => {
  const {
    applications,
    page,
    perPage,
    setPage,
    setPerPage,
    totalItems,
    refetch,
  } = useApplications(true);

  const { deleteApplication } = useDeleteApplication(refetch);

  if (!applications) return <CircularProgress color="primary" />;

  const handleDelete = (id: string) => {
    if (window.confirm("Are you sure want to delete this application ?")) {
      deleteApplication(id);
    }
  };

  return (
    <>
      <RootContainer>
        <TableContainer>
          <TableHead>
            <TableRow>
              {headers.map((header, index) => (
                <TableCell key={index}>
                  <TableSortLabel>{header}</TableSortLabel>
                </TableCell>
              ))}
            </TableRow>
          </TableHead>
          <TableBody>
            {applications.map((application) => (
              <TableRow key={application.id}>
                <TableCell>
                  {moment(application.createdAt).format(
                    "MMMM Do YYYY, h:mm:ss a"
                  )}
                </TableCell>
                <TableCell>{application.candidate.name}</TableCell>
                <TableCell>{application.candidate.email}</TableCell>
                <TableCell>{application.job.title}</TableCell>
                <TableCell>
                  {application.job.description.slice(0, 40)}
                </TableCell>
                <TableCell>
                  <Button
                    color="secondary"
                    variant="outlined"
                    onClick={() => handleDelete(application.id)}
                  >
                    Delete
                  </Button>
                </TableCell>
              </TableRow>
            ))}
          </TableBody>
        </TableContainer>
        <TablePagination
          rowsPerPageOptions={[5, 10, 25]}
          component="div"
          count={totalItems}
          rowsPerPage={perPage}
          page={page}
          onPageChange={(_, page) => setPage(page)}
          onRowsPerPageChange={(e) => setPerPage(parseInt(e.target.value))}
        />
      </RootContainer>
    </>
  );
};
