import { RadioGroup } from "@material-ui/core";
import styled from "styled-components";

export const CustomRadioGroup = styled(RadioGroup)`
  flex-direction: row;
  margin: auto;
`;
