import {
  TableContainer,
  TableBody,
  TableCell,
  TableHead,
  TablePagination,
  TableRow,
  CircularProgress,
  Button,
  Toolbar,
  FormControlLabel,
  Radio,
} from "@material-ui/core";
import moment from "moment";
import React from "react";
import { useAllUsers } from "../../../../hooks/users/useAllUsers";
import { useDeleteUser } from "../../../../hooks/users/useDeleteUser";
import { IUser } from "../../../../types";
import { CustomRadioGroup } from "./styled";

export const Users: React.FC = () => {
  const headers = ["Created At", "Updated At", "Email", "Name", "Actions"];
  const {
    users,
    totalItems,
    perPage,
    page,
    isLoading,
    role,
    changePerPage,
    fetchPage,
    changeRole,
    refetch,
  } = useAllUsers();
  const { deleteUser, isLoading: deleteUserLoading } = useDeleteUser({
    onDelete: refetch,
  });

  if (isLoading) return <CircularProgress />;

  const handleDelete = (id: string, name: string) => {
    if (window.confirm(`Do you want to delete ${name} ?`)) {
      deleteUser(id);
    }
  };

  return (
    <TableContainer>
      <Toolbar>
        <CustomRadioGroup
          value={role}
          onChange={(e) => changeRole(e.target.value as IUser["role"])}
        >
          <FormControlLabel value="" label="All" control={<Radio />} />
          <FormControlLabel
            value="CANDIDATE"
            label="Candidate"
            control={<Radio />}
          />
          <FormControlLabel
            value="RECRUITER"
            label="Recruiter"
            control={<Radio />}
          />
          <FormControlLabel value="ADMIN" label="Admin" control={<Radio />} />
        </CustomRadioGroup>
      </Toolbar>
      <TableHead>
        <TableRow>
          {headers.map((header, index) => (
            <TableCell key={index}>{header}</TableCell>
          ))}
        </TableRow>
      </TableHead>
      <TableBody>
        {users.map((user) => (
          <TableRow key={user.id}>
            <TableCell>
              {moment(user.createdAt).format("MMMM Do YYYY, h:mm:ss a")}
            </TableCell>
            <TableCell>
              {moment(user.updatedAt).format("MMMM Do YYYY, h:mm:ss a")}
            </TableCell>
            <TableCell>{user.email}</TableCell>
            <TableCell>{user.name}</TableCell>
            <TableCell>
              <Button
                disabled={deleteUserLoading}
                onClick={() => handleDelete(user.id, user.name)}
                variant="outlined"
                color="secondary"
              >
                Delete
              </Button>
            </TableCell>
          </TableRow>
        ))}
      </TableBody>
      <TablePagination
        count={totalItems}
        page={page}
        rowsPerPage={perPage}
        rowsPerPageOptions={[5, 10, 20, 30]}
        onPageChange={(_, value) => fetchPage(value)}
        onRowsPerPageChange={(e) => changePerPage(parseInt(e.target.value))}
      />
    </TableContainer>
  );
};
