import React from "react";
import { useForm, SubmitHandler } from "react-hook-form";
import { TextField, Button, Typography } from "@material-ui/core";
import AccountCircleIcon from "@material-ui/icons/AccountCircle";
import { yupResolver } from "@hookform/resolvers/yup";
import ReCaptcha from "react-google-recaptcha";
import * as yup from "yup";

import { Navbar } from "../../../components/Navbar/Navbar";
import FormContainer from "../../../components/FormContainer";
import { registerTextField } from "../../../utils";
import { useLogin } from "../../../hooks/auth/useLogin";
import { ILoginFormFields } from "../../../types";
import { emailField, requiredField } from "../../../validators/fields";
import { HtmlTitle } from "../../../components/HtmlTitle";
import { useNotification } from "../../../hooks/useNotification";

export const loginSchema = yup.object().shape({
  email: emailField,
  password: requiredField,
});

export const AdminLogin: React.FC = () => {
  const {
    register,
    handleSubmit,
    setError,
    formState: { errors },
  } = useForm<ILoginFormFields>({
    resolver: yupResolver(loginSchema),
  });

  const { showNotification } = useNotification();

  const { isLoading, login } = useLogin(setError, { isAdmin: true });

  const [reToken, setReToken] = React.useState<string | null>("");

  const onSubmit: SubmitHandler<ILoginFormFields> = async (data) => {
    if (!reToken) {
      showNotification("Please submit the captcha", "warning");
      return;
    }

    login({ ...data, reToken });
  };

  return (
    <>
      <HtmlTitle title="Admin login" description="Login for admin" />
      <Navbar>
        <form onSubmit={handleSubmit(onSubmit)}>
          <FormContainer>
            <AccountCircleIcon
              style={{ margin: "0 auto", fontSize: 50 }}
              color="secondary"
            />
            <Typography variant="h5" align="center">
              Login to your account
            </Typography>

            <TextField
              {...registerTextField(register, errors, "email")}
              label="Email"
              variant="outlined"
            />

            <TextField
              {...registerTextField(register, errors, "password")}
              label="Password"
              type="password"
              variant="outlined"
            />

            <ReCaptcha
              sitekey={process.env.REACT_APP_RECAPTCHA_PUBLIC_KEY || ""}
              onChange={(token) => setReToken(token)}
              size="normal"
            />

            <Button
              disabled={isLoading}
              type="submit"
              color="secondary"
              variant="contained"
            >
              {isLoading ? "Loading ..." : "Login"}
            </Button>
          </FormContainer>
        </form>
      </Navbar>
    </>
  );
};
