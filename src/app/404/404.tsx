import { Typography } from "@material-ui/core";
import React from "react";
import styled from "styled-components";
import NotFoundImage from "../../assets/404.svg";
import { HtmlTitle } from "../../components/HtmlTitle";

export const PageNotFound: React.FC = () => {
  return (
    <Container>
      <HtmlTitle title="Page not found" description="404 Page not found" />
      <Image src={NotFoundImage} />
      <Typography variant="h5">Page not found</Typography>
    </Container>
  );
};

const Container = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  padding: 50px;
  min-height: 100vh;
`;

const Image = styled.img`
  max-width: 400px;
  width: 100%;
  margin-bottom: 20px;
`;
