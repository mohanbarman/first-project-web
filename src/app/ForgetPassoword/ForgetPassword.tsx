import { yupResolver } from "@hookform/resolvers/yup";
import { Button, TextField, Typography } from "@material-ui/core";
import React from "react";
import { SubmitHandler } from "react-hook-form";
import { useForm } from "react-hook-form";
import FormContainer from "../../components/FormContainer";
import Navbar from "../../components/Navbar";
import { registerTextField } from "../../utils";
import { forgetPasswordSchema } from "./validationSchema";
import { useForgetPassword } from "../../hooks/auth/useForgetPassword";
import { IForgetPasswordFormFields } from "../../types";
import { HtmlTitle } from "../../components/HtmlTitle";

export const ForgetPassword: React.FC = () => {
  const {
    register,
    formState: { errors },
    handleSubmit,
    setError,
  } = useForm<IForgetPasswordFormFields>({
    resolver: yupResolver(forgetPasswordSchema),
  });

  const { forgetPassword, isLoading } = useForgetPassword(setError);

  const onSubmit: SubmitHandler<IForgetPasswordFormFields> = (data) => {
    forgetPassword(data);
  };

  return (
    <>
      <HtmlTitle title="Forgot password" description="Forgot your password" />
      <Navbar>
        <form onSubmit={handleSubmit(onSubmit)}>
          <FormContainer>
            <Typography variant="h5" align="center">
              Forgot your password
            </Typography>

            <TextField
              {...registerTextField(register, errors, "email")}
              label="Email Address"
              variant="outlined"
            />

            <Button
              disabled={isLoading}
              type="submit"
              color="primary"
              variant="contained"
            >
              {isLoading ? "Loading..." : "Submit"}
            </Button>
          </FormContainer>
        </form>
      </Navbar>
    </>
  );
};
