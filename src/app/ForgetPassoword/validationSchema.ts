import * as yup from "yup";
import { emailField } from "../../validators";

export const forgetPasswordSchema = yup.object().shape({
  email: emailField,
});
