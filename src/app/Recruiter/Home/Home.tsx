import { Tab, Tabs } from "@material-ui/core";
import React from "react";
import styled from "styled-components";
import { HtmlTitle } from "../../../components/HtmlTitle";

import Navbar from "../../../components/Navbar";
import { Applications } from "./Applications";
import { RecruiterJobs } from "./Jobs";

export const RecruiterHome: React.FC = () => {
  const [tab, setTab] = React.useState(0);

  return (
    <>
      <HtmlTitle title="Home" description="Homepage for recruiter" />
      <Navbar>
        <Container>
          <Tabs
            value={tab}
            indicatorColor="primary"
            textColor="primary"
            onChange={(_, value) => setTab(value)}
          >
            <Tab label="My Applications" />
            <Tab label="My Jobs" />
          </Tabs>
          {tab === 0 && <Applications />}
          {tab === 1 && <RecruiterJobs />}
        </Container>
      </Navbar>
    </>
  );
};

const Container = styled.div`
  margin-top: 20px;
  display: flex;
  justify-content: center;
  align-items: center;
  flex-direction: column;
  gap: 30px;

  .MuiTableContainer-root {
    width: unset;
  }
`;
