import {
  CircularProgress,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
} from "@material-ui/core";
import moment from "moment";
import React from "react";
import styled from "styled-components";

import { useAllJobs } from "../../../../hooks/job/useAllJobs";
import { useAppSelector } from "../../../../redux/hooks";
import { userSelector } from "../../../../redux/slices/userSlice";

const headers = ["Created At", "Updated At", "Title", "Description"];

export const RecruiterJobs: React.FC = () => {
  const user = useAppSelector(userSelector);
  const { jobs, isLoading } = useAllJobs({ recruiterId: user.id });

  if (isLoading) return <CircularProgress />;

  return (
    <Container>
      <TableContainer>
        <TableHead>
          <TableRow>
            {headers.map((header) => (
              <TableCell>{header}</TableCell>
            ))}
          </TableRow>
        </TableHead>
        <TableBody>
          {jobs.map((job) => (
            <TableRow>
              <TableCell>
                {moment(job.createdAt).format("MMMM Do YYYY, h:mm:ss a")}
              </TableCell>
              <TableCell>
                {moment(job.updatedAt).format("MMMM Do YYYY, h:mm:ss a")}
              </TableCell>
              <TableCell>{job.title}</TableCell>
              <TableCell>{job.description.slice(0, 50)}</TableCell>
            </TableRow>
          ))}
        </TableBody>
      </TableContainer>
    </Container>
  );
};

export const Container = styled.div`
  margin-top: 20px;
  display: flex;
  justify-content: center;
  align-items: center;
  flex-direction: column;
  gap: 30px;

  .MuiTableContainer-root {
    width: unset;
  }
`;
