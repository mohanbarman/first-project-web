import { yupResolver } from "@hookform/resolvers/yup";
import {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  TextField,
} from "@material-ui/core";
import React from "react";
import { SubmitHandler } from "react-hook-form";
import { useForm } from "react-hook-form";
import * as yup from "yup";

import { useAddJob } from "../../../../hooks/job/useAddJob";
import { registerTextField } from "../../../../utils";
import { requiredField } from "../../../../validators";

interface IProps {
  isOpen: boolean;
  onClose: () => void;
}

interface IFormFields {
  title: string;
  description: string;
}

const validator = yup.object().shape({
  title: requiredField.min(5).max(200),
  description: requiredField.min(20).max(1000),
});

export const AddJob: React.FC<IProps> = ({ isOpen, onClose }) => {
  const {
    register,
    handleSubmit,
    reset: resetForm,
    formState: { errors },
  } = useForm<IFormFields>({ resolver: yupResolver(validator) });

  const { createJob, isLoading, isSuccess, reset } = useAddJob();

  const onSubmit: SubmitHandler<IFormFields> = (data) => {
    createJob(data);
  };

  if (isSuccess) {
    onClose();
    reset();
    resetForm();
  }

  const handleClose = () => {
    reset();
    resetForm();
    onClose();
  };

  return (
    <Dialog open={isOpen} onClose={handleClose}>
      <form onSubmit={handleSubmit(onSubmit)}>
        <DialogTitle>Add a new job</DialogTitle>
        <DialogContent>
          <TextField
            {...registerTextField(register, errors, "title")}
            autoFocus
            variant="outlined"
            label="Title"
            disabled={isLoading}
            fullWidth
            style={{ marginBottom: "20px" }}
          />
          <TextField
            {...registerTextField(register, errors, "description")}
            multiline
            minRows={5}
            variant="outlined"
            label="Description"
            disabled={isLoading}
            fullWidth
            style={{ marginBottom: "30px" }}
          />
          <DialogActions>
            <Button disabled={isLoading} onClick={handleClose}>
              Cancel
            </Button>
            <Button
              type="submit"
              disabled={isLoading}
              color="primary"
              variant="contained"
            >
              Create
            </Button>
          </DialogActions>
        </DialogContent>
      </form>
    </Dialog>
  );
};
