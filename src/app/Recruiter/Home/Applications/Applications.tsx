import {
  Typography,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  TableSortLabel,
  Toolbar,
  TablePagination,
  CircularProgress,
  Button,
} from "@material-ui/core";
import React from "react";
import moment from "moment";

import { Container, RootContainer } from "./styled";
import { useApplications } from "../../../../hooks/applications/useAllApplications";
import { useState } from "react";
import { AddJob } from "../AddJob";

const headers = [
  "Applied At",
  "Candidate Name",
  "Candidate Email",
  "Job Title",
  "Job Description",
];

export const Applications: React.FC = () => {
  const { applications, page, perPage, setPage, setPerPage, totalItems } =
    useApplications();

  const [isAddJobDialogOpen, setIsAddJobDialogOpen] = useState(false);

  if (!applications) return <CircularProgress color="primary" />;

  return (
    <>
      <AddJob
        isOpen={isAddJobDialogOpen}
        onClose={() => setIsAddJobDialogOpen(false)}
      />
      <RootContainer>
        <Container elevation={6}>
          <TableContainer>
            <Toolbar>
              <Typography variant="h5" id="tableTitle" component="div">
                All Applications
              </Typography>
              <Button
                style={{ marginLeft: "auto" }}
                color="primary"
                variant="contained"
                onClick={() => setIsAddJobDialogOpen(true)}
              >
                Add new job
              </Button>
            </Toolbar>
            <TableHead>
              <TableRow>
                {headers.map((header, index) => (
                  <TableCell key={index}>
                    <TableSortLabel>{header}</TableSortLabel>
                  </TableCell>
                ))}
              </TableRow>
            </TableHead>
            <TableBody>
              {applications.map((application) => (
                <TableRow key={application.id}>
                  <TableCell>
                    {moment(application.createdAt).format(
                      "MMMM Do YYYY, h:mm:ss a"
                    )}
                  </TableCell>
                  <TableCell>{application.candidate.name}</TableCell>
                  <TableCell>{application.candidate.email}</TableCell>
                  <TableCell>{application.job.title}</TableCell>
                  <TableCell>
                    {application.job.description.slice(0, 40)}
                  </TableCell>
                </TableRow>
              ))}
            </TableBody>
          </TableContainer>
          <TablePagination
            rowsPerPageOptions={[5, 10, 25]}
            component="div"
            count={totalItems}
            rowsPerPage={perPage}
            page={page}
            onPageChange={(_, page) => setPage(page)}
            onRowsPerPageChange={(e) => setPerPage(parseInt(e.target.value))}
          />
        </Container>
      </RootContainer>
    </>
  );
};
