import { Paper } from "@material-ui/core";
import styled from "styled-components";

export const Container = styled(Paper)`
  margin: auto;
`;

export const RootContainer = styled.div`
  margin-top: 20px;
  display: flex;
  justify-content: center;
`;
