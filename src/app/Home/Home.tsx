import React from "react";
import { HtmlTitle } from "../../components/HtmlTitle";
import { Navbar } from "../../components/Navbar/Navbar";
import { Content } from "./Content";

export const Home: React.FC = () => {
  return (
    <>
      <HtmlTitle title="My Jobs" description="My jobs homepage" />
      <Navbar>
        <Content />
      </Navbar>
    </>
  );
};
