import styled from "styled-components";

export const HomeContentContainer = styled.div`
  min-height: calc(100vh - 400px);
  display: flex;
  gap: 30px;
  max-width: 1000px;
  width: 100%;
  margin: auto;
  align-items: center;
  justify-content: space-between;

  @media (max-width: 900px) {
    flex-direction: column-reverse;
    text-align: center;
  }
`;

export const HomeContentTextContainer = styled.div`
  display: flex;
  flex-direction: column;
  flex: 1;
  h4 {
    margin-bottom: 10px;
  }
  h5 {
    margin-bottom: 30px;
  }
`;

export const HomeContentIllustration = styled.img`
  max-width: 500px;
  width: 100%;
`;

export const HomeContentButtonsContainer = styled.div`
  display: flex;
  gap: 20px;

  @media (max-width: 900px) {
    justify-content: center;
  }
`;
