import { Button, Typography } from "@material-ui/core";
import React from "react";
import {
  HomeContentButtonsContainer,
  HomeContentContainer,
  HomeContentIllustration,
  HomeContentTextContainer,
} from "./styled";
import illustrationSrc from "../../assets/mobile.svg";
import { data } from "./data";

export const Content: React.FC = () => {
  return (
    <HomeContentContainer>
      <HomeContentTextContainer>
        <Typography variant="h4">{data.heading}</Typography>
        <Typography variant="h5">{data.subheading}</Typography>
        <HomeContentButtonsContainer>
          <Button color="primary" variant="contained" size="large">
            Play Store
          </Button>
          <Button color="primary" variant="contained" size="large">
            App Store
          </Button>
        </HomeContentButtonsContainer>
      </HomeContentTextContainer>
      <HomeContentIllustration src={illustrationSrc} />
    </HomeContentContainer>
  );
};
