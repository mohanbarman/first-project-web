import * as yup from "yup";
import { emailField, passwordField, requiredField } from "../../validators";

export const registerSchema = yup.object().shape({
  email: emailField,
  password: passwordField,
  confirmPassword: requiredField.oneOf(
    [yup.ref("password")],
    "Password didn't matched"
  ),
  name: requiredField,
});
