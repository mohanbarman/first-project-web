import React from "react";
import { useForm, SubmitHandler } from "react-hook-form";
import {
  TextField,
  Button,
  Typography,
  FormControlLabel,
  Radio,
  FormControl,
  FormLabel,
  RadioGroup,
} from "@material-ui/core";
import AccountCircleIcon from "@material-ui/icons/AccountCircle";
import { Link } from "react-router-dom";
import { yupResolver } from "@hookform/resolvers/yup";

import { Navbar } from "../../components/Navbar/Navbar";
import { FormContainer } from "../../components/FormContainer/FormContainer";
import { registerSchema } from "./registerSchema";
import { registerTextField } from "../../utils";
import { useRegister } from "../../hooks/auth/useRegister";
import { IRegisterFormFields, IUser } from "../../types";
import { HtmlTitle } from "../../components/HtmlTitle";

export const Register: React.FC = () => {
  const {
    register,
    handleSubmit,
    setError,
    formState: { errors },
  } = useForm<IRegisterFormFields>({
    resolver: yupResolver(registerSchema),
  });

  const [role, setRole] = React.useState<IUser["role"]>("CANDIDATE");

  const { register: registerUser, isLoading } = useRegister(setError);

  const onSubmit: SubmitHandler<IRegisterFormFields> = async (data) => {
    registerUser({ ...data, role });
  };

  return (
    <>
      <HtmlTitle title="Register" description="Register for a new account" />
      <Navbar>
        <form onSubmit={handleSubmit(onSubmit)}>
          <FormContainer>
            <AccountCircleIcon
              style={{ margin: "0 auto", fontSize: 50 }}
              color="secondary"
            />
            <Typography variant="h5" align="center">
              Create an account
            </Typography>

            <TextField
              {...registerTextField(register, errors, "name")}
              label="Full Name"
              variant="outlined"
            />

            <TextField
              {...registerTextField(register, errors, "email")}
              label="Email"
              variant="outlined"
            />

            <TextField
              {...registerTextField(register, errors, "password")}
              label="Password"
              type="password"
              variant="outlined"
            />

            <TextField
              {...registerTextField(register, errors, "confirmPassword")}
              label="Confirm Password"
              type="password"
              variant="outlined"
            />

            <FormControl component="fieldset">
              <FormLabel component="legend">Role</FormLabel>
              <RadioGroup
                row
                value={role}
                onChange={(e) => setRole(e.target.value as IUser["role"])}
              >
                <FormControlLabel
                  value="CANDIDATE"
                  label="Candidate"
                  control={<Radio color="primary" />}
                />
                <FormControlLabel
                  value="RECRUITER"
                  label="Recruiter"
                  control={<Radio color="primary" />}
                />
              </RadioGroup>
            </FormControl>

            <Button
              disabled={isLoading}
              type="submit"
              color="primary"
              variant="contained"
            >
              {isLoading ? "Loading..." : "Register"}
            </Button>
            <Typography align="center">
              Already have an account ?
              <Link to="/login">
                <Button
                  color="primary"
                  style={{ textTransform: "capitalize", padding: 0 }}
                >
                  Login
                </Button>
              </Link>
            </Typography>
          </FormContainer>
        </form>
      </Navbar>
    </>
  );
};
