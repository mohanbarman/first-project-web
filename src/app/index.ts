export * from "./Home";
export * from "./Candidate";
export * from "./Recruiter";
export * from "./ForgetPassoword";
export * from "./admin";
export * from "./404";
export { ResetPassword } from "./ResetPassword";
export { Register } from "./Register";
export { Login } from "./Login";
