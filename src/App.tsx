import { ThemeProvider } from "@material-ui/core";

import Notification from "./components/Notification";
import Router from "./Router";
import { theme } from "./theme";
import { GlobalStyles } from "./theme/globalStyles";

export const App = () => {
  return (
    <ThemeProvider theme={theme}>
      <GlobalStyles />
      <Notification />
      <Router />
    </ThemeProvider>
  );
};
