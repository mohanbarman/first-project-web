import React from "react";
import { Switch, Route } from "react-router-dom";

import {
  AdminLogin,
  AdminHome,
  AppliedJobs,
  CandidateHome,
  ForgetPassword,
  Home,
  Login,
  RecruiterHome,
  Register,
  ResetPassword,
  PageNotFound,
} from "./app";
import { useAppSelector } from "./redux/hooks";
import { CenteredCircularLoader } from "./components/CenteredCircularLoader";
import { userSelector } from "./redux/slices/userSlice";
import { useAuth } from "./hooks/useAuth";
import { useEffect } from "react";

const Router: React.FC = () => {
  const user = useAppSelector(userSelector);
  const { status, getMe } = useAuth();

  useEffect(() => {
    getMe();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  if (status === "loading") {
    return <CenteredCircularLoader />;
  }

  return (
    <Switch>
      <Route path="/" exact={true}>
        <Home />
      </Route>
      {status === "unauthenticated" && [
        <Route path="/login" exact={true}>
          <Login />
        </Route>,
        <Route path="/admin/login" exact={true}>
          <AdminLogin />
        </Route>,
        <Route path="/register" exact={true}>
          <Register />
        </Route>,
        <Route path="/forgetPassword" exact={true}>
          <ForgetPassword />
        </Route>,
        <Route path="/resetPassword/:secretCode" exact={true}>
          <ResetPassword />
        </Route>,
      ]}
      {status === "authenticated" && [
        <Route path="/app" exact={true}>
          {user.role === "CANDIDATE" && <CandidateHome />}
          {user.role === "RECRUITER" && <RecruiterHome />}
          {user.role === "ADMIN" && <AdminHome />}
        </Route>,
      ]}
      {status === "authenticated" && user.role === "CANDIDATE" && (
        <Route path="/appliedJobs" exact={true}>
          <AppliedJobs />
        </Route>
      )}
      <Route>
        <PageNotFound />
      </Route>
    </Switch>
  );
};

export default Router;
