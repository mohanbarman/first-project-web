import { FieldError, UseFormRegister } from "react-hook-form";

/**
 * Helper to easily bind react-hook-form errors to material ui TextField component
 * @param name
 * @param errors
 */
export const registerError = <T extends Record<string, FieldError>>(
  errors: T,
  name: keyof typeof errors
) => ({
  error: !!errors[name],
  helperText: errors[name]?.message || "",
});

/**
 * Wrapper of react-hook-form register function
 * @param register
 * @param errors
 * @param name
 */
export const registerTextField = <
  T extends UseFormRegister<Record<string, string>>,
  E extends Record<string, FieldError>
>(
  register: T,
  errors: E,
  name: keyof typeof errors
) => ({
  inputProps: register(name as string),
  error: !!errors[name],
  helperText: errors[name]?.message || "",
});
