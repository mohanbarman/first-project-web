export const getObjectByKey = <T>(key: string): T | {} => {
  const data = localStorage.getItem(key);
  if (!data) return {};

  const parsedData = JSON.parse(data);
  return parsedData;
};

export const setObjectByKey = <T extends Record<string, any>>(
  key: string,
  data: T
): boolean => {
  localStorage.setItem(key, JSON.stringify(data));
  return true;
};
